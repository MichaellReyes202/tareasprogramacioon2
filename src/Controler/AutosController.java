package Controler;

import Models.Auto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import views.AutoFrame;
import views.RegisterAuto;

public class AutosController implements ActionListener, ItemListener {

    private RegisterAuto ra;
    private AutoFrame aframe;
    private Auto auto;
    private int std;
    JFileChooser d;

    public AutosController() {

    }

    public AutosController(AutoFrame frame) {
        d = new JFileChooser();
        aframe = frame;
    }

    public AutosController(RegisterAuto reg) {
        ra = reg;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {

            case "OPEN" -> {
                std = d.showOpenDialog(aframe);
                if (std == JFileChooser.APPROVE_OPTION) {
                    auto = open(d.getSelectedFile());
                    aframe.openACT(auto);
                } else if (std == JFileChooser.CANCEL_OPTION) {
                }
            }
            case "SAVE" -> {
                std = d.showSaveDialog(aframe);
                if (std == JFileChooser.APPROVE_OPTION) {
                    File file = d.getSelectedFile();
                    auto = aframe.saveACT();
                    if (save(file)) {
                        JOptionPane.showMessageDialog(aframe, "Se ha guardado con exito");
                    }
                } else if (std == JFileChooser.CANCEL_OPTION) {
                }
            }
            case "Borrar" -> {
                aframe.setRegistro().clear();
            }
            case "EXIT" -> {
                System.exit(0);
            }

        }
    } // fin de actionPerformed

    public boolean save(File file) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(auto);
            w.flush();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public Auto open(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Auto) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

}

package Models;

import java.io.Serializable;

public class Auto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Marca;
    private String Modelo;
    private int Anno;

    public String getEmicion() {
        return Emicion;
    }

    public void setEmicion(String Emicion) {
        this.Emicion = Emicion;
    }

    public String getOtros() {
        return Otros;
    }

    public void setOtros(String Otros) {
        this.Otros = Otros;
    }

    private String Emicion;
    private String Otros;

    private String Color;
    private String Chasis;
    private String Placa;
    private String VIN; // vehicle identification number

    private String Gravamen; //  es un certificado que acredita que el vehículo en cuestión posee algún embargo, prenda vehicular, juicio u otro tipo de situaciones 

    private int Npasajeros;
    private double NTonelaje;
    private int NCilindros;

    private String TipoOtroCombustible;

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public int getAnno() {
        return Anno;
    }

    public void setAnno(int Anno) {
        this.Anno = Anno;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getChasis() {
        return Chasis;
    }

    public void setChasis(String Chasis) {
        this.Chasis = Chasis;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getGravamen() {
        return Gravamen;
    }

    public void setGravamen(String Gravamen) {
        this.Gravamen = Gravamen;
    }

    public int getNpasajeros() {
        return Npasajeros;
    }

    public void setNpasajeros(int Npasajeros) {
        this.Npasajeros = Npasajeros;
    }

    public double getNTonelaje() {
        return NTonelaje;
    }

    public void setNTonelaje(double NTonelaje) {
        this.NTonelaje = NTonelaje;
    }

    public int getNCilindros() {
        return NCilindros;
    }

    public void setNCilindros(int NCilindros) {
        this.NCilindros = NCilindros;
    }

    public int getTCombustible() {
        return TCombustible;
    }

    public void setTCombustible(int TCombustible) {
        this.TCombustible = TCombustible;
    }

    public int getTUso() {
        return TUso;
    }

    public void setTUso(int TUso) {
        this.TUso = TUso;
    }

    public int getTServicio() {
        return TServicio;
    }

    public void setTServicio(int TServicio) {
        this.TServicio = TServicio;
    }

    public String getTipoOtroCombustible() {
        return TipoOtroCombustible;
    }

    public void setTipoOtroCombustible(String TipoOtroCombustible) {
        this.TipoOtroCombustible = TipoOtroCombustible;
    }

    private int TCombustible; //    private int TipoCombustible; 
    private int TUso;  // particular , taxi , bus
    private int TServicio; // privado , Publico

}
